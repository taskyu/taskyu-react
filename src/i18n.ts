import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import LanguageDetector from 'i18next-browser-languagedetector'

import es from 'locales/es.json'

const resources = { es }

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    defaultNS: 'common',
    resources,
    fallbackLng: 'es',
    lng: 'es',
    debug: false,
    keySeparator: false,
    interpolation: {
      escapeValue: false
    }
  })

export default i18n
