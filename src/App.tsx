import React from 'react'
import { ReactQueryCacheProvider } from 'react-query'
import { ToastContainer } from 'react-toastify'
import { AuthContextProvider } from 'views/contexts/AuthContext/AuthContext'

import Router from './Router'

function App() {
  return (
    <main className="App">
      <ReactQueryCacheProvider>
        <AuthContextProvider>
          <Router />
        </AuthContextProvider>
      </ReactQueryCacheProvider>
      <ToastContainer
        position="bottom-right"
        limit={3}
        autoClose={3000}
      />
    </main>
  )
}

export default App
