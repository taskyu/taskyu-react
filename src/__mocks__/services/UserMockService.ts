import { AccessToken, User, UserLogin } from 'models'
import { UserService } from 'services/interfaces/UserService'
import { AccessTokenMock, UserMock } from '__mocks__/models'
import fakeQuery from '__mocks__/utils/fakeQuery'

class UserMockService implements UserService {
  async findById(_userId: string) {
    return fakeQuery<User>(UserMock)
  }

  async login(user: UserLogin) {
    console.log(user)
    return fakeQuery<AccessToken>(AccessTokenMock)
  }

  async refresh() {
    return fakeQuery<AccessToken>(AccessTokenMock)
  }

  async create(user: User) {
    return fakeQuery<User>(user)
  }
}

export default UserMockService
