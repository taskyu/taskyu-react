interface Options<T> {
  delay: number
  reject: boolean
  onResolve: (t?: T) => void
  onReject: (t?: T) => void
}

const defaultOptions: Options<unknown> = {
  delay: 300,
  reject: false,
  onResolve: () => {},
  onReject: () => {}
}

/**
 * Función para falsear retorno de promesas
 */
const fakeQuery = <T extends unknown>(
  returnValue?: T,
  options: Options<T> = defaultOptions
) : Promise<T> => {
  return new Promise((resolve, reject) => {
    const mergedOptions = { ...defaultOptions, ...options }
    setTimeout(() => {
      if (mergedOptions.reject) {
        mergedOptions.onReject(returnValue)
        reject(returnValue)
      } else {
        mergedOptions.onResolve(returnValue)
        resolve(returnValue)
      }
    }, mergedOptions.delay)
  })
}

export default fakeQuery
