import { User } from 'models'

const UserMock: User = {
  id: '1aee63c2-c39a-41e7-ac79-af3a2cdf60ca',
  validated: true,
  birth: '1993-08-07',
  firstname: 'Boris',
  lastname: 'Belmar',
  email: 'test@gmail.com',
  createdAt: 1600550644,
  updatedAt: 1600550644
}

export default UserMock
