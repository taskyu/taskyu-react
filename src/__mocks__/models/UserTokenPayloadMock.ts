import { UserTokenPayload } from 'models'

const UserTokenPayloadMock: UserTokenPayload = {
  exp: 1600709271,
  iat: 1600622871,
  sub: '1aee63c2-c39a-41e7-ac79-af3a2cdf60ca',
  validated: true,
  firstname: 'Boris',
  lastname: 'Belmar',
  email: 'test@gmail.com'
}

export default UserTokenPayloadMock
