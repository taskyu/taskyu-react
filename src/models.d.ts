export interface AccessToken {
  accessToken: string
}

export interface BaseEntity {
  id?: string
  createdAt?: number
  updatedAt?: number
}

export interface Category extends BaseEntity {
  name: string
  description?: string
  color: string
  workspace?: string
}

export interface List extends BaseEntity {
  name: string
  slug?: string
  description?: string
  color?: string
  workspace?: Workspace | string
}

export interface Task extends BaseEntity {
  name: string
  description?: string
  deadline: number
  isDone: boolean
  list?: List | string
  subTasks?: SubTask[]
  assigned: User[]
  categories: Category[]
  createdBy?: User | string
  updatedBy?: User | string
}

export interface SubTask extends BaseEntity {
  name: string
  isDone: boolean
}

export interface User extends BaseEntity {
  email: string
  firstname: string
  lastname: string
  birth?: string
  password?: string
  validated?: boolean
}

export interface UserLogin {
  email: string
  password: string
}

export interface UserTokenPayload {
  sub: string
  exp: number
  iat: number
  email: string
  firstname: string
  lastname: string
  validated: boolean
}

export interface Workspace extends BaseEntity {
  name: string
  slug?: string
  description?: string
  owner?: User
  members?: User[]
}

export interface StatsTask {
  count: number
  overdue: number
  pending: number
  done: number
}

export interface StatsUserWorkspace extends Workspace {
  tasks: StatsTask
}

export interface StatsUser {
  email?: string
  firstname?: string
  lastname?: string
  workspaces: number | StatsUserWorkspace[]
  tasks: StatsTask
}

export interface StatsList extends List {
  tasks: StatsTask
}

export interface StatsWorkspace extends Workspace {
  members: number | StatsUser[]
  lists: number | StatsList[]
  tasks: StatsTask
}
