import * as Yup from 'yup'
import { TFunction } from 'i18next'
import { YYYYMMDD_REGEX } from 'constants/regex'

const getUserValidationSchema = (t: TFunction) => Yup.object().shape({
  email: Yup.string()
    .email(t('validations:EMAIL'))
    .required(t('validations:REQUIRED')),
  firstname: Yup.string()
    .min(3, t('validations:MIN_STRING', { number: 3 }))
    .max(20, t('validations:MAX_STRING', { number: 20 }))
    .required(t('validations:REQUIRED')),
  lastname: Yup.string()
    .min(3, t('validations:MIN_STRING', { number: 3 }))
    .max(20, t('validations:MAX_STRING', { number: 20 }))
    .required(t('validations:REQUIRED')),
  birth: Yup.string()
    .matches(YYYYMMDD_REGEX, { message: t('validations:DATE') })
    .required(t('validations:REQUIRED')),
  password: Yup.string()
    .min(6, t('validations:MIN_STRING', { number: 6 }))
    .required(t('validations:REQUIRED')),
  password_repeat: Yup.string()
    .oneOf([Yup.ref('password'), undefined], t('validations:PASSWORD_MATCH'))
    .required(t('validations:REQUIRED'))
})

export default getUserValidationSchema
