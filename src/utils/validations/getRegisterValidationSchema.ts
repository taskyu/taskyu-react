import * as Yup from 'yup'
import { TFunction } from 'i18next'
import { dateStringTransformer } from 'views/components/DateInput/utils'

const getRegisterValidationSchema = (t: TFunction) => Yup.object().shape({
  email: Yup.string()
    .email(t('validations:EMAIL'))
    .required(t('validations:REQUIRED')),
  firstname: Yup.string()
    .min(3, t('validations:MIN_STRING', { number: 3 }))
    .max(20, t('validations:MAX_STRING', { number: 20 }))
    .required(t('validations:REQUIRED')),
  lastname: Yup.string()
    .min(3, t('validations:MIN_STRING', { number: 3 }))
    .max(20, t('validations:MAX_STRING', { number: 20 }))
    .required(t('validations:REQUIRED')),
  birth: Yup.date()
    .transform(dateStringTransformer)
    .max(new Date(), t('validations:BEFORE_TODAY'))
    .min(new Date('1900-01-01'), t('validations:MIN_DATE', { date: '1900-01-01' }))
    .required(t('validations:REQUIRED')),
  password: Yup.string()
    .min(6, t('validations:MIN_STRING', { number: 6 }))
    .required(t('validations:REQUIRED')),
  password_repeat: Yup.string()
    .oneOf([Yup.ref('password'), undefined], t('validations:PASSWORD_MATCH'))
    .required(t('validations:REQUIRED')),
  terms: Yup.boolean()
    .oneOf([true], t('validations:MUST_ACCEPT_TERMS'))
})

export default getRegisterValidationSchema
