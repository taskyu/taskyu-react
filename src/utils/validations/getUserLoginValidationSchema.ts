import * as Yup from 'yup'
import { TFunction } from 'i18next'

const getUserLoginValidationSchema = (t: TFunction) => Yup.object().shape({
  email: Yup.string()
    .email(t('validations:EMAIL'))
    .required(t('validations:REQUIRED')),
  password: Yup.string()
    .min(6, t('validations:MIN_STRING', { number: 6 }))
    .required(t('validations:REQUIRED'))
})

export default getUserLoginValidationSchema
