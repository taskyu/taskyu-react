import tailwindConfig from 'tailwind.config'
import { pathOr } from 'ramda'
import configResolver from './configResolver'

const twConfig = configResolver(tailwindConfig)

export const { theme } = twConfig

export const getColor = (color: string) => (
  (variant: string): string => (
    pathOr('', [color, variant], theme.colors)
  )
)
