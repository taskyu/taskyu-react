import resolveConfig from 'tailwindcss/resolveConfig'

// Refactor this to Typescript!

const configResolver = tailwindConfigFile => resolveConfig(tailwindConfigFile)

export default configResolver
