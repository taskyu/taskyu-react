import { faChartPie, faCog, faCube, faList, faTags, faTasks, faUsers } from '@fortawesome/free-solid-svg-icons'
import { IconDefinition } from '@fortawesome/fontawesome-svg-core'

export interface NavigationItem {
  to: string
  label: string
  icon: IconDefinition
  owner?: boolean
  user?: boolean
}

const navigationConfig: NavigationItem[] = [
  {
    to: '/',
    label: 'MY_TASKS',
    icon: faTasks,
    user: true
  },
  {
    to: '/workspaces',
    label: 'MY_WORKSPACES',
    icon: faCube,
    user: true
  },
  {
    to: '/list',
    label: 'LISTS',
    icon: faList
  },
  {
    to: '/categories',
    label: 'CATEGORIES',
    icon: faTags
  },
  {
    to: '/statistics',
    label: 'STATISTICS',
    icon: faChartPie
  },
  {
    to: '/members',
    label: 'MEMBERS',
    icon: faUsers
  },
  {
    to: '/preferences',
    label: 'PREFERENCES',
    icon: faCog,
    owner: true
  }
]

export default navigationConfig
