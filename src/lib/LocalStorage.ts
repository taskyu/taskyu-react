export const STORAGE_PREFIX = 'taskyu::'
export const STORAGE_TOKEN = 'accessToken'

/**
 * Capa de abstracción para el localStorage
 */
class LocalStorage {
  private static instance: LocalStorage

  private storage: Storage | undefined

  private constructor() {
    try {
      this.storage = window.localStorage
    } catch (e) {
      console.error(e)
      console.error('LocalStorage is not present on your browser')
    }
  }

  public static getInstance() {
    if (!LocalStorage.instance) {
      LocalStorage.instance = new LocalStorage()
    }
    return LocalStorage.instance
  }

  get(key: string) {
    return this.storage?.getItem(STORAGE_PREFIX + key)
  }

  set(key: string, value: string) {
    this.storage?.setItem(STORAGE_PREFIX + key, value)
  }

  remove(key: string) {
    this.storage?.removeItem(STORAGE_PREFIX + key)
  }

  key(index: number) {
    return this.storage?.key(index)
  }

  clear() {
    this.storage?.clear()
  }

  getAccessToken() {
    return this.get(STORAGE_TOKEN)
  }

  setAccessToken(token: string) {
    this.set(STORAGE_TOKEN, token)
  }

  clearAccessToken() {
    this.remove(STORAGE_TOKEN)
  }

  clearUserData() {
    // Aquí van todos los remove correspondientes a la información del usuario
    this.clearAccessToken()
  }
}

export default LocalStorage
