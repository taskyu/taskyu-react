import api from 'lib/api'
import { AccessToken, User, UserLogin } from 'models'
import { UserService } from 'services/interfaces'

// TODO: Improving error management
class UserApiService implements UserService {
  async findById(userId: string) {
    const response = await api.get<User>(`/user/${userId}`)
    return response.data
  }

  async login(user: UserLogin) {
    const response = await api.post<AccessToken>('/user/login', user)
    return response.data
  }

  async refresh() {
    const response = await api.get<AccessToken>('/user/refresh')
    return response.data
  }

  async create(user: User) {
    const response = await api.post<User>('/user/register', user)
    return response.data
  }
}

export default UserApiService
