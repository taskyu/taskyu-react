import { ServiceProvider } from 'services/interfaces/ServiceProvider'
import UserApiService from './UserApiService'

const ApiServiceProvider: ServiceProvider = {
  userService: new UserApiService()
}

export default ApiServiceProvider
