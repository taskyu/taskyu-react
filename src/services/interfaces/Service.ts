export interface Service<T> {
  findAll?: () => Promise<T[]>
  findById: (id: string) => Promise<T>
  create: (t: T) => Promise<T>
  update?: (t: T) => Promise<T>
  delete?: (id: string) => Promise<T>
}

export interface StrictService<T> extends Service<T> {
  findAll: () => Promise<T[]>
  update: (t: T) => Promise<T>
  delete: (id: string) => Promise<T>
}
