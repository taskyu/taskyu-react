import { AccessToken, User, UserLogin } from 'models'
import { Service } from './Service'

export interface UserService extends Service<User> {
  login: (user: UserLogin) => Promise<AccessToken>
  refresh: () => Promise<AccessToken>
}
