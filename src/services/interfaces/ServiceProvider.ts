import { UserService } from './UserService'

export interface ServiceProvider {
  userService: UserService
}
