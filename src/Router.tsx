import React, { Suspense } from 'react'
import { Router as ReactRouter, Switch, Route } from 'react-router-dom'
import { AnimatePresence } from 'framer-motion'
import { createBrowserHistory } from 'history'
import { MyTasks, Login, Register, NotFound, UserProfile, SuspenseLoading, Terms } from 'views/pages'
import ProtectedRoute from 'views/components/ProtectedRoute'
import NavigationLayout from 'views/layouts/NavigationLayout/NavigationLayout'

export const history = createBrowserHistory()

function Router() {
  return (
    <ReactRouter history={history}>
      <AnimatePresence>
        <Switch>
          <Route exact path={['/login', '/register', '/terms']}>
            <Suspense fallback={<SuspenseLoading isScreenfull />}>
              <Switch>
                <ProtectedRoute type="public-only" exact path="/login" Page={Login} />
                <ProtectedRoute type="public-only" exact path="/register" Page={Register} />
                <ProtectedRoute type="public-only" exact path="/terms" Page={Terms} />
              </Switch>
            </Suspense>
          </Route>
          <Route>
            <NavigationLayout>
              <Suspense fallback={<SuspenseLoading />}>
                <Switch>
                  <ProtectedRoute exact path="/" Page={MyTasks} />
                  <ProtectedRoute exact path="/profile" Page={UserProfile} />
                  <ProtectedRoute Page={NotFound} />
                </Switch>
              </Suspense>
            </NavigationLayout>
          </Route>
        </Switch>
      </AnimatePresence>
    </ReactRouter>
  )
}

export default Router
