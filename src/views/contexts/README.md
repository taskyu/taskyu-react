# Contexts

En este directorio van todos los componentes de tipo contexto, es decir, que proveen lógica y datos a componentes de menor jerarquía.


```tsx
const [count, setCount] = useState(0)

const addCount = () => setCount(count => count + 1)

<AuthContext>
  <ProtectedPage />
</AuthContext>
```