import { Reducer, useCallback, useEffect, useReducer } from 'react'
import JwtDecode from 'jwt-decode'
import LocalStorage from 'lib/LocalStorage'
import { User } from 'models'
import api from 'lib/api'
import { ActionTypes, AuthAction, AuthState, IAuthContext } from './types.d'

const token = LocalStorage.getInstance().getAccessToken()

const userPlaceholder = { email: '', firstname: '', lastname: '' }

const initialState: AuthState = {
  accessToken: token,
  user: token ? JwtDecode(token) : userPlaceholder
}

const reducer: Reducer<AuthState, AuthAction> = (state, action) => {
  switch (action.type) {
    case ActionTypes.authenticate:
      return action.accessToken ? {
        accessToken: action.accessToken,
        user: action.user || state.user
      } : state
    case ActionTypes.logout:
      return {
        accessToken: undefined,
        user: userPlaceholder
      }
    default:
      throw new Error()
  }
}

const useAuthContextLogic = (): IAuthContext => {
  const [state, dispatch] = useReducer(reducer, initialState)

  const authenticate = useCallback((accessToken: string) => {
    LocalStorage.getInstance().setAccessToken(accessToken)
    const decodedToken = JwtDecode<User>(accessToken)
    dispatch({
      type: ActionTypes.authenticate,
      accessToken,
      user: decodedToken
    })
    return decodedToken
  }, [])

  const logout = useCallback(() => {
    LocalStorage.getInstance().clearUserData()
    dispatch({ type: ActionTypes.logout })
  }, [])

  useEffect(() => {
    // TODO: Better way?
    api.defaults.headers.common.Authorization = state.accessToken ? `Bearer ${state.accessToken}` : ''
  }, [state.accessToken])

  return {
    isAuthenticated: !!state.accessToken,
    user: state.user,
    authenticate,
    logout
  }
}

export default useAuthContextLogic
