import React, { createContext, useContext } from 'react'
import { AuthContextProps, IAuthContext } from './types.d'
import useAuthContextLogic from './useAuthContextLogic'

const AuthContext = createContext<IAuthContext>({
  isAuthenticated: false,
  logout: () => {},
  authenticate: _accessToken => undefined,
  user: {
    email: '',
    firstname: '',
    lastname: ''
  }
})

export const AuthContextProvider = ({ children }: AuthContextProps) => {
  const contextValues = useAuthContextLogic()
  return (
    <AuthContext.Provider value={contextValues}>
      {children}
    </AuthContext.Provider>
  )
}

export const useAuthContext = () => useContext(AuthContext)
