import { User } from 'models'
import { ReactNode } from 'react'

export interface AuthState {
  accessToken?: string | null
  user: User
}

export interface IAuthContext {
  isAuthenticated: boolean
  user: User
  logout: () => void
  authenticate: (accessToken: string) => User | undefined
}

export interface AuthAction {
  type: string
  accessToken?: string
  user?: User
}

export interface AuthContextProps {
  children: ReactNode
}

export enum ActionTypes {
  authenticate = 'AUTHENTICATE',
  logout = 'LOGOUT'
}
