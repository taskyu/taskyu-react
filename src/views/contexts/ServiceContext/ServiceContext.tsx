import React, { createContext, ReactNode, useContext } from 'react'
import ApiServiceProvider from 'services/api/ApiServiceProvider'
import { ServiceProvider } from 'services/interfaces'

const ServiceContext = createContext<ServiceProvider>(ApiServiceProvider)

interface Props {
  children: ReactNode,
  serviceProvider?: ServiceProvider
}

export const ServiceContextProvider = ({ children, serviceProvider }: Props) => {
  return (
    <ServiceContext.Provider value={serviceProvider || ApiServiceProvider}>
      {children}
    </ServiceContext.Provider>
  )
}

export const useServiceContext = () => useContext(ServiceContext)
