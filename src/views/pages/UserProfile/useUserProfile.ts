import { User } from 'models'
import { yupResolver } from '@hookform/resolvers'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import getUserValidationSchema from 'utils/validations/getUserValidationSchema'

const useUserProfile = () => {
  const { t } = useTranslation()
  const { handleSubmit, register, errors } = useForm<User>({
    resolver: yupResolver(getUserValidationSchema(t))
  })

  const onSubmit = (data: User) => {
    console.log(data)
  }

  return {
    handleSubmit: handleSubmit(onSubmit),
    register,
    errors
  }
}

export default useUserProfile
