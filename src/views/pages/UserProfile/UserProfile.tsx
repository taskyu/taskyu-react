import { faUserCog } from '@fortawesome/free-solid-svg-icons'
import React from 'react'
import { useTranslation } from 'react-i18next'
import Button from 'views/components/Button/Button'
import TextInput from 'views/components/TextInput/TextInput'
import { useAuthContext } from 'views/contexts/AuthContext/AuthContext'
import PageLayout from 'views/layouts/PageLayout'
import useUserProfile from './useUserProfile'

const UserProfile = () => {
  const { handleSubmit, errors, register } = useUserProfile()
  const { t } = useTranslation('navigation')
  const { logout } = useAuthContext()

  return (
    <PageLayout
      icon={faUserCog}
      title={t('MY_PROFILE')}
      description={t('MY_PROFILE_DESC')}>
      <h1>User Profile</h1>
      <form onSubmit={handleSubmit}>
        <TextInput
          name="email"
          label="Email"
          type="email"
          errors={errors}
          register={register} />
        <div className="flex">
          <Button
            text="Guardar"
            type="submit" />
          <Button
            className="ml-2"
            text="Cerrar sesión"
            type="button"
            color="secondary"
            onClick={logout} />
        </div>
      </form>
    </PageLayout>
  )
}

export default UserProfile
