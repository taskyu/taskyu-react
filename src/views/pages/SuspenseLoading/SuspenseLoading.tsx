import clsx from 'clsx'
import React from 'react'
import { motion } from 'framer-motion'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'

interface Props {
  isScreenfull?: boolean
}

const SuspenseLoading = ({ isScreenfull }: Props) => (
  <motion.div animate={{ opacity: [0, 2] }} transition={{ duration: 0.1 }}>
    <div className={clsx(['flex flex-col w-full items-center justify-center', isScreenfull ? 'h-screen' : 'h-full'])}>
      <motion.div
        animate={{ scale: [0.5, 1, 2, 1, 0.5], rotate: [0, 359, 0] }}
        transition={{ duration: 1, loop: Infinity }}>
        <FontAwesomeIcon icon={faSpinner} className="text-5xl text-primary-500 mb-2" />
      </motion.div>
      <h3>Loading...</h3>
    </div>
  </motion.div>
)

export default SuspenseLoading
