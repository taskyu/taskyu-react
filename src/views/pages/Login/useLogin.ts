import { UserLogin } from 'models'
import { yupResolver } from '@hookform/resolvers'
import { useForm } from 'react-hook-form'
import { useServiceContext } from 'views/contexts'
import { useAuthContext } from 'views/contexts/AuthContext/AuthContext'
import { omit } from 'ramda'
import { toast } from 'react-toastify'
import { useTranslation } from 'react-i18next'
import getUserLoginValidationSchema from 'utils/validations/getUserLoginValidationSchema'
import LocalStorage from 'lib/LocalStorage'

interface UserLoginExtended extends UserLogin {
  remember: boolean
}

const useLogin = () => {
  const { t } = useTranslation()
  const { userService } = useServiceContext()
  const { authenticate } = useAuthContext()
  const { handleSubmit, register, errors } = useForm<UserLoginExtended>({
    resolver: yupResolver(getUserLoginValidationSchema(t))
  })

  const onSubmit = async (data: UserLoginExtended) => {
    const payload = omit(['remember'], data)
    try {
      const accessToken = await userService.login(payload)

      if (data.remember) {
        LocalStorage.getInstance().set('email_remember', data.email)
      } else {
        LocalStorage.getInstance().remove('email_remember')
      }

      const user = authenticate(accessToken.accessToken)
      toast.success(t('WELCOME', user))
    } catch (e) {
      toast.error(
        t(`errors:${e.response?.data?.msg || e.message}`)
      )
    }
  }

  const handleLogin = handleSubmit(onSubmit)

  return { handleLogin, register, errors }
}

export default useLogin
