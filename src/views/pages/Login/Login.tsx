import React, { useRef } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import LocalStorage from 'lib/LocalStorage'
import Button from 'views/components/Button'
import CheckboxInput from 'views/components/CheckboxInput/CheckboxInput'
import TextInput from 'views/components/TextInput'
import AuthLayout from 'views/layouts/AuthLayout/AuthLayout'
import useLogin from './useLogin'

const Login = () => {
  const { t } = useTranslation('auth')
  const { handleLogin, errors, register } = useLogin()
  const rememberedEmail = useRef(LocalStorage.getInstance().get('email_remember') as string | undefined)

  return (
    <AuthLayout>
      <div className="mb-12">
        <h1 className="text-4xl leading-relaxed">{t('LOGIN_TITLE')}</h1>
        <p>{t('LOGIN_REGISTER_QUESTION')}<Link className="text-primary-500 hover:text-primary-600" to="/register">{t('LOGIN_REGISTER_LINK')}</Link></p>
      </div>
      <form onSubmit={handleLogin}>
        <div className="mb-8">
          <TextInput
            name="email"
            type="email"
            label={t('EMAIL_LABEL')}
            placeholder={t('EMAIL_PLACEHOLDER')}
            defaultValue={rememberedEmail.current}
            register={register}
            errors={errors} />
          <TextInput
            name="password"
            label={t('PASSWORD_LABEL')}
            type="password"
            register={register}
            errors={errors} />
          <CheckboxInput
            className="mt-6"
            name="remember"
            label={t('REMEMBER_ME')}
            defaultChecked={!!rememberedEmail.current}
            register={register}
            errors={errors} />
        </div>
        <Button
          text={t('LOGIN_BUTTON')}
          size="lg"
          type="submit"
          isFullwidth />
      </form>
    </AuthLayout>
  )
}

export default Login
