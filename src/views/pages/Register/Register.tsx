import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import Button from 'views/components/Button'
import CheckboxInput from 'views/components/CheckboxInput/CheckboxInput'
import DateInput from 'views/components/DateInput/DateInput'
import TextInput from 'views/components/TextInput'
import AuthLayout from 'views/layouts/AuthLayout/AuthLayout'
import useRegister from './useRegister'

const Login = () => {
  const { t } = useTranslation('auth')
  const { handleRegister, errors, register, setValue } = useRegister()

  return (
    <AuthLayout>
      <div className="mb-12">
        <h1 className="text-4xl leading-relaxed">
          {t('REGISTER_TITLE')}
        </h1>
        <p>{t('REGISTER_LOGIN_QUESTION')}<Link className="text-primary-500 hover:text-primary-600" to="/login">{t('REGISTER_LOGIN_LINK')}</Link></p>
      </div>
      <form onSubmit={handleRegister}>
        <div className="mb-8">
          <TextInput
            name="email"
            type="email"
            label={t('EMAIL_LABEL')}
            placeholder={t('EMAIL_PLACEHOLDER')}
            register={register}
            errors={errors} />
          <TextInput
            name="firstname"
            label={t('FIRSTNAME_LABEL')}
            register={register}
            errors={errors} />
          <TextInput
            name="lastname"
            label={t('LASTNAME_LABEL')}
            register={register}
            errors={errors} />
          <DateInput
            name="birth"
            label="Nacimiento"
            errors={errors}
            beforeToday
            setValue={setValue}
            register={register} />
          <TextInput
            name="password"
            label={t('PASSWORD_LABEL')}
            type="password"
            register={register}
            errors={errors} />
          <TextInput
            name="password_repeat"
            label={t('PASSWORD_REPEAT_LABEL')}
            type="password"
            register={register}
            errors={errors} />
          <CheckboxInput
            name="terms"
            className="mt-6"
            register={register}
            errors={errors}>
            {t('ACCEPT_THE')} <Link to="/terms" className="text-primary-500 hover:text-primary-600 transition duration-150">{t('TERMS')}</Link>
          </CheckboxInput>
        </div>
        <Button text="Registrarse" size="lg" type="submit" isFullwidth />
      </form>
    </AuthLayout>
  )
}

export default Login
