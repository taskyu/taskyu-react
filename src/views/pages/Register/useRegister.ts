import { User } from 'models'
import { yupResolver } from '@hookform/resolvers'
import { useForm } from 'react-hook-form'
import { useServiceContext } from 'views/contexts'
import { omit } from 'ramda'
import { toast } from 'react-toastify'
import { useTranslation } from 'react-i18next'
import getRegisterValidationSchema from 'utils/validations/getRegisterValidationSchema'
import { useHistory } from 'react-router'

const useRegister = () => {
  const { t } = useTranslation()
  const { userService } = useServiceContext()
  const { push } = useHistory()
  const { handleSubmit, register, errors, setValue } = useForm<User>({
    resolver: yupResolver(getRegisterValidationSchema(t))
  })

  const onSubmit = async (data: User) => {
    const payload = omit(['password_repeat', 'terms'], data)
    try {
      // TODO: Add email validation use case
      await userService.create(payload)
      push('/login')
      toast.success(
        t('Tu cuenta se ha creado con éxito')
      )
    } catch (e) {
      if (e.response?.status === 409) {
        toast.error(
          t('errors:EXISTENT_USER')
        )
      } else {
        t(`errors:${e.response?.data?.msg || e.message}`)
      }
    }
  }

  const handleRegister = handleSubmit(onSubmit)

  return { handleRegister, register, errors, setValue }
}

export default useRegister
