import React from 'react'
import { useHistory } from 'react-router'
import Button from 'views/components/Button/Button'
import PageLayout from 'views/layouts/PageLayout/PageLayout'

const NotFound = () => {
  const { push } = useHistory()
  const handleClick = () => {
    push('/')
  }
  return (
    <PageLayout className="flex items-center justify-center h-screen">
      <div className="text-center">
        <h2 className="text-2xl mb-12">Ups... No encontramos lo que estabas buscando</h2>
        <Button onClick={handleClick} text="Volver al inicio" />
      </div>
    </PageLayout>
  )
}

export default NotFound
