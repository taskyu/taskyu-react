import React from 'react'

export const Login = React.lazy(() => import('./Login'))
export const MyTasks = React.lazy(() => import('./MyTasks'))
export const NotFound = React.lazy(() => import('./NotFound'))
export const Register = React.lazy(() => import('./Register'))
export const UserProfile = React.lazy(() => import('./UserProfile'))
export const Terms = React.lazy(() => import('./Terms'))
export { default as SuspenseLoading } from './SuspenseLoading'
