import React from 'react'
import { useTranslation } from 'react-i18next'
import { useAuthContext } from 'views/contexts/AuthContext/AuthContext'
import PageLayout from 'views/layouts/PageLayout'

const MyTasks = () => {
  const { t } = useTranslation()
  const { logout, user } = useAuthContext()

  return (
    <PageLayout label="MY_TASKS">
      <p>{t('WELCOME', user)}</p>
      <button onClick={logout}>Logout</button>
    </PageLayout>
  )
}

export default MyTasks
