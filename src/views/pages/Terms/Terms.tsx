/* eslint-disable max-len */
import React from 'react'
import { useHistory } from 'react-router'
import Button from 'views/components/Button/Button'

const Terms = () => {
  const { goBack } = useHistory()

  const handleClick = () => {
    goBack()
  }

  return (
    <div className="p-8 md:p-12 sm:p-16 lg:w-7/12 w-full mx-auto">
      <h1 className="text-3xl font-bold text-gray-800 mb-4">Términos y condiciones</h1>
      <div className="text-gray-700 leading-loose">
        <p className="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam aliquet, lacus vitae elementum vulputate, libero sem luctus libero, sed tincidunt lorem lacus eu enim. Integer nisl massa, faucibus efficitur lorem quis, commodo ultrices ligula. Suspendisse sit amet risus quis nibh feugiat volutpat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum porttitor lacus sit amet augue tempus, et sollicitudin diam dapibus. Nunc elit lorem, pulvinar a semper non, viverra ut lectus. Etiam rhoncus aliquet tellus sit amet efficitur. Pellentesque ac odio ut nunc accumsan efficitur vitae in tellus. Donec tempus tempus lectus, in auctor odio pharetra a.</p>
        <p className="mb-4">Ut in accumsan libero, ac bibendum leo. Aenean vehicula porttitor diam, at scelerisque neque faucibus quis. In ultricies semper lorem ut viverra. Mauris eget risus id elit vulputate hendrerit eget vel turpis. In mollis posuere mi, a finibus risus lobortis quis. Curabitur dignissim felis nec diam venenatis hendrerit mollis eget nisl. Aliquam erat volutpat. In posuere arcu vel mollis ultricies.</p>
        <p className="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sodales nibh egestas dui tristique vestibulum. Suspendisse ultricies urna ligula, malesuada malesuada est luctus ut. Phasellus sed pretium lectus. Donec eget commodo leo. Proin ultrices est pharetra massa porttitor, ac fringilla lectus ultricies. Maecenas venenatis vitae lorem nec pretium. Aliquam vehicula sit amet ligula eu gravida. Integer sodales, eros porttitor viverra mollis, ex orci aliquet tortor, sit amet scelerisque orci mauris sit amet lorem. Curabitur porta mi auctor purus laoreet euismod.</p>
        <Button text="Volver" size="md" onClick={handleClick} />
      </div>
    </div>
  )
}

export default Terms
