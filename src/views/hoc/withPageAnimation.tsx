import React from 'react'
import { motion } from 'framer-motion'

const withPageAnimation = (Component: React.ComponentType<unknown>) => () => {
  return (
    <motion.div animate={{ opacity: [0, 1], scale: [0.9, 1] }} transition={{ duration: 0.25 }}>
      <Component />
    </motion.div>
  )
}

export default withPageAnimation
