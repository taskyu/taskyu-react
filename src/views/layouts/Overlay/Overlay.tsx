import React, { RefObject, useRef } from 'react'
import { motion } from 'framer-motion'
import clsx from 'clsx'
import useClickOverlay from './useClickOverlay'
import { ILayout } from '../ILayout'

interface OverlayProps extends ILayout {
  onClose: () => void
  options?: {
    notCentered: boolean
  }
}

const Overlay = ({ children, onClose, options }: OverlayProps) => {
  const innerRef = useRef<HTMLDivElement>(null)

  useClickOverlay(innerRef, onClose)

  return (
    <motion.div
      id="overlay"
      animate={{ opacity: 1 }}
      initial={{ opacity: 0 }}
      exit={{ opacity: 0 }}
      className={clsx(
        ['w-full h-full top-0 left-0 bg-opacity-50 bg-black flex fixed'],
        !options?.notCentered && 'items-center justify-center'
      )}>
      <div ref={innerRef as RefObject<HTMLDivElement>}>
        {children}
      </div>
    </motion.div>
  )
}

export default Overlay
