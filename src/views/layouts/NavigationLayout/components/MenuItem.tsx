import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link, useLocation } from 'react-router-dom'
import clsx from 'clsx'
import { IMenuItem } from '../models'

const MenuItem = ({ to, icon, label, className, onClick }: IMenuItem) => {
  const { pathname } = useLocation()

  return (
    <Link
      to={to}
      onClick={onClick}
      className={clsx([
        'flex px-2 py-2 rounded items-center text-white bg-transparent hover:bg-gray-900 hover:bg-opacity-25 transition duration-200',
        to === pathname && 'bg-gray-700 bg-opacity-25',
        className
      ])}>
      <FontAwesomeIcon icon={icon} className="text-xl" fixedWidth />
      <p className="ml-2 text-lg">{label}</p>
    </Link>
  )
}

export default MenuItem
