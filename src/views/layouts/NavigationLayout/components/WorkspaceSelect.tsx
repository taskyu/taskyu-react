import React from 'react'
import { useTranslation } from 'react-i18next'
import Select, { Props } from 'react-select'
import { navbarSelectStyle, selectTheme } from 'views/components/SelectInput/styles'

const WorkspaceSelect = ({ value, ...props }: Props) => {
  const { t } = useTranslation('navigation')
  return (
    <Select
      styles={navbarSelectStyle}
      theme={selectTheme}
      placeholder={t('SELECT_WORKSPACE_PLACEHOLDER')}
      noOptionsMessage={input => t('SELECT_WORKSPACE_NO_OPTION', input)}
      value={value}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props} />
  )
}

export default React.memo(WorkspaceSelect)
