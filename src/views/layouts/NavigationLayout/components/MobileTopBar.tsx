import React from 'react'
import { faBars, faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

interface MobileTopBarProps {
  onClickMenu: () => void
  workspaceName?: string
}

const MobileTopBar = ({ onClickMenu, workspaceName }: MobileTopBarProps) => {
  return (
    <div className="sticky top-0 w-full h-12 text-white bg-primary-500 flex items-stretch justify-between shadow-md">
      <button onClick={onClickMenu} className="flex items-center md:px-8 px-4">
        <FontAwesomeIcon icon={faBars} className="text-2xl" />
      </button>
      <div className="md:px-8 px-4 flex items-center">
        <h4 className="font-bold mr-2 leading-none">Taskyu</h4>
        <p className="text-sm opacity-50 leading-none mr-2 ">v{`${process.env.REACT_APP_VERSION}`}</p>
        {workspaceName && (
          <>
            <p className="mr-2">|</p>
            <p className="font-semibold leading-none">{workspaceName}</p>
          </>
        )}
      </div>
      <button className="flex items-center md:px-8 px-4">
        <FontAwesomeIcon icon={faPlus} className="text-2xl" />
      </button>
    </div>
  )
}

export default React.memo(MobileTopBar)
