import React from 'react'
import { User } from 'models'
import ProfileIcon from 'views/components/ProfileIcon/ProfileIcon'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCog } from '@fortawesome/free-solid-svg-icons'
import { Link, useLocation } from 'react-router-dom'
import clsx from 'clsx'

interface NavProfileCardProps {
  user: User
  to: string
  onPressed?: () => void
}

const MenuProfileCard = ({ user, to, onPressed }: NavProfileCardProps) => {
  const { pathname } = useLocation()
  return (
    <Link
      to={to}
      onClick={onPressed}
      className={clsx([
        'flex p-2 rounded items-center justify-between text-white bg-transparent hover:bg-gray-900 hover:bg-opacity-25 transition duration-200',
        to === pathname && 'bg-gray-700 bg-opacity-25'
      ])}>
      <div className="flex items-center">
        <ProfileIcon user={user} variant="light" className="mr-3" />
        <div>
          <h4 className="font-bold leading-tight truncate w-40">{`${user?.firstname} ${user?.lastname}`}</h4>
          <p className="text-sm truncate w-40 lg:w-48">{user?.email}</p>
        </div>
      </div>
      <FontAwesomeIcon icon={faCog} className="text-lg block" />
    </Link>
  )
}

export default MenuProfileCard
