import React from 'react'
import { useTranslation } from 'react-i18next'
import Separator from 'views/components/Separator/Separator'
import { MenuProps } from '../models'
import MenuItem from './MenuItem'
import MenuProfileCard from './MenuProfileCard'
import WorkspaceSelect from './WorkspaceSelect'

const Menu = ({
  user,
  navigation,
  selectedWorkspace,
  onWorkspaceSelect,
  workspaces,
  onItemClick
}: MenuProps) => {
  const { t } = useTranslation('navigation')

  return (
    <div className="lg:w-84 w-72 bg-gradient-to-b from-primary-500 to-secondary-500 h-screen py-8 px-4 shadow-lg sm:sticky top-0 fixed">
      <MenuProfileCard
        user={user}
        to="/profile"
        onPressed={onItemClick} />
      <Separator />
      {navigation.getUserNavigation().map(item => (
        <MenuItem
          key={item.to}
          to={item.to}
          icon={item.icon}
          label={t(item.label)}
          className="mb-2"
          onClick={onItemClick} />
      ))}
      <Separator />
      <WorkspaceSelect
        options={workspaces}
        onChange={onWorkspaceSelect}
        value={selectedWorkspace} />
      <Separator />
      {selectedWorkspace && navigation.getWorkspaceNavigation(selectedWorkspace).map(item => (
        <MenuItem
          key={item.to}
          to={item.to}
          icon={item.icon}
          label={t(item.label)}
          className="mb-2"
          onClick={onItemClick} />
      ))}
    </div>
  )
}

export default React.memo(Menu)
