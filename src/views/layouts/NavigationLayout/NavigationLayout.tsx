import React, { useCallback, useMemo, useState } from 'react'
import usePortal from 'react-useportal'
import { ValueType } from 'react-select'
import { useAuthContext } from 'views/contexts/AuthContext/AuthContext'
import useWindowSize from 'views/hooks/useWindowSize'
import navigationConfig from 'config/navigationConfig'
import { AnimatePresence } from 'framer-motion'
import { ILayout } from '../ILayout'
import Menu from './components/Menu'
import { WorkspaceOption } from './models'
import Navigation from './models/Navigation'
import Overlay from '../Overlay/Overlay'
import MobileTopBar from './components/MobileTopBar'

const workspaces: WorkspaceOption[] = [
  { value: '1aee63c2-c39a-41e7-ac79-af3a2cdf60ca', label: 'Mi Espacio' },
  { value: '1aee63c2-c39a-41e7-ac79-af3a2cdf60cb', label: 'Algún Workspace' },
  { value: '1aee63c2-c39a-41e7-ac79-af3a2cdf60cc', label: 'ACME Inc.' }
]

const NavigationLayout = ({ children }: ILayout) => {
  const { user } = useAuthContext()
  const { isMobile } = useWindowSize()
  const { Portal, isOpen, openPortal, closePortal } = usePortal()
  const [selectedWorkspace, setSelectedWorkspace] = useState<WorkspaceOption>()

  const handleWorkspaceSelect = useCallback((selectedOption: ValueType<WorkspaceOption>) => {
    setSelectedWorkspace(selectedOption as WorkspaceOption)
  }, [])

  const navigation = useMemo(
    () => new Navigation(navigationConfig, user),
    [user]
  )

  return (
    <>
      {isMobile && (
        <MobileTopBar
          onClickMenu={openPortal}
          workspaceName={selectedWorkspace?.label} />
      )}
      <div className="flex flex-row">
        <div className="flex-1">
          {!isMobile && (
          <Menu
            user={user}
            navigation={navigation}
            selectedWorkspace={selectedWorkspace}
            onWorkspaceSelect={handleWorkspaceSelect}
            workspaces={workspaces}
            />
          )}
        </div>
        <div className="w-full">
          {children}
        </div>
      </div>
      <AnimatePresence>
        {isOpen && isMobile && (
        <Portal>
          <Overlay onClose={closePortal} options={{ notCentered: true }}>
            <Menu
              user={user}
              navigation={navigation}
              selectedWorkspace={selectedWorkspace}
              onWorkspaceSelect={handleWorkspaceSelect}
              workspaces={workspaces}
              onItemClick={closePortal}
              />
          </Overlay>
        </Portal>
        )}
      </AnimatePresence>
    </>
  )
}

export default NavigationLayout
