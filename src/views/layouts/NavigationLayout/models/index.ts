import { IconDefinition } from '@fortawesome/fontawesome-svg-core'
import { User } from 'models'
import { ValueType } from 'react-select'

export interface INavigation {
  getUserNavigation: () => IMenuItem[]
  getWorkspaceNavigation: (workspace: WorkspaceOption) => IMenuItem[]
}

export interface IMenuItem {
  to: string
  label: string
  icon: IconDefinition
  className?: string
  onClick?: () => void
  owner?: boolean
}

export interface WorkspaceOption {
  value: string
  label: string
}

export interface MenuProps {
  user: User
  navigation: INavigation
  selectedWorkspace?: WorkspaceOption
  onWorkspaceSelect: (selectedOption: ValueType<WorkspaceOption>) => void
  workspaces: WorkspaceOption[]
  onItemClick?: () => void
}
