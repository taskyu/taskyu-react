import { User } from 'models'
import { NavigationItem } from 'config/navigationConfig'
import { INavigation, WorkspaceOption } from '.'

class Navigation implements INavigation {
  private user?: User

  private navigation: NavigationItem[]

  constructor(navigation: NavigationItem[], user?: User) {
    this.user = user
    this.navigation = navigation
  }

  private appendWorkspaceId(workspaceId: string) {
    return (item: NavigationItem) => {
      const path = '/'.concat(workspaceId.concat(item.to))
      return { ...item, to: path }
    }
  }

  public getUserNavigation() {
    return this.navigation.filter(item => item.user)
  }

  // TODO: IMPLEMENT THIS!
  // Must return filtered navigation for workspace based on user and owner required
  public getWorkspaceNavigation(workspace: WorkspaceOption) {
    return this.navigation.filter(item => !item.user).map(this.appendWorkspaceId(workspace.value))
  }
}

export default Navigation
