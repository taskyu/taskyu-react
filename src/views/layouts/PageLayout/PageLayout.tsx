import React, { useMemo } from 'react'
import clsx from 'clsx'
import { useTranslation } from 'react-i18next'
import { find, propEq } from 'ramda'
import navigationConfig, { NavigationItem } from 'config/navigationConfig'
import { IconDefinition } from '@fortawesome/fontawesome-svg-core'
import { ILayout } from '../ILayout'
import PageTitle from './components/PageTitle'

interface PageLayoutProps extends ILayout {
  label?: string
  title?: string
  description?: string
  icon?: IconDefinition
  className?: string
}

const getItemByLabel = (label?:string) => {
  return find<NavigationItem>(propEq('label', label))(navigationConfig)
}

const PageLayout = ({ children, label, title, description, icon, className }: PageLayoutProps) => {
  const { t } = useTranslation('navigation')
  const pageItem = useMemo(() => getItemByLabel(label), [label])

  return (
    <>
      <PageTitle
        icon={icon || pageItem?.icon}
        title={title || (label && t(label))}
        description={description || (label && t(`${label}_DESC`))}
      />
      <main className={clsx(['p-4 md:p-8', className])}>
        {children}
      </main>
    </>
  )
}

export default PageLayout
