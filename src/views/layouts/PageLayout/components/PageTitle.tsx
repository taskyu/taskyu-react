import { IconProp } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'

interface PageTitleProps {
  icon?: IconProp
  title: string | undefined
  description?: string
}

const PageTitle = ({ icon, title, description }: PageTitleProps) => {
  return title ? (
    <div className="flex h-24 bg-gray-100 shadow-md">
      <div className="md:px-8 px-4 flex-1 flex items-center">
        {icon && (
          <div className="bg-primary-500 shadow rounded h-12 w-12 flex items-center justify-center mr-4 text-2xl text-white">
            <FontAwesomeIcon icon={icon} />
          </div>
        )}
        <div>
          <h1 className="text-xl font-bold text-gray-700 leading">{title}</h1>
          <p className="text-sm text-gray-600">{description}</p>
        </div>
      </div>
    </div>
  ) : null
}

export default PageTitle
