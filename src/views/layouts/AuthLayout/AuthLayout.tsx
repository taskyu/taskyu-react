import React from 'react'
import authBgImage from 'assets/img/auth-bg.jpg'
import { ILayout } from '../ILayout'

const AuthLayout = ({ children }: ILayout) => {
  return (
    <div className="flex min-h-screen">
      <div className="2xl:p-32 lg:p-16 p-8 sm:w-6/12 lg:w-5/12 xl:w-4/12 w-full">
        {children}
      </div>
      <div className="relative top-0 sm:w-6/12 lg:w-7/12 xl:w-8/12 w-0">
        <div className="absolute top-0 left-0 w-full h-full z-10 bg-gradient-to-br from-primary-500 to-secondary-500 opacity-75" />
        <div className="absolute top-0 left-0 w-full h-full z-0 bg-cover bg-center" style={{ backgroundImage: `url(${authBgImage}` }} />
      </div>
    </div>
  )
}

export default AuthLayout
