import { useCallback, useEffect, useRef, useState } from 'react'

interface WindowSize {
  width: number
  height: number
  isMobile: boolean
}

const getSize = (): WindowSize => {
  const width = document.documentElement.clientWidth
  const height = document.documentElement.clientHeight
  return { width, height, isMobile: width <= 720 }
}

const useWindowSize = (delay = 100) => {
  const [windowSize, setWindowSize] = useState<WindowSize>(getSize())
  const resizeTimeout = useRef<number>()

  const resizeCallback = useCallback(() => {
    clearTimeout(resizeTimeout.current)
    resizeTimeout.current = window.setTimeout(() => {
      setWindowSize(getSize())
    }, delay)
  }, [delay])

  useEffect(() => {
    window.addEventListener('resize', resizeCallback)
    return () => {
      window.removeEventListener('resize', resizeCallback)
    }
  }, [resizeCallback])

  return windowSize
}

export default useWindowSize
