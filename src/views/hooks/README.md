# Hooks

En este directorio van todos los hooks, los cuales son lógica de componentes encapsulada.


```typescript
const useCount = () => {
  const [count, setCount] = useState(0)

  const addCount = () => setCount(count => count + 1)

  return { count, addCount }
}

const makeUser = (name, id): User => { name: name, id: id }

const { count, addCount } = useCount()
```