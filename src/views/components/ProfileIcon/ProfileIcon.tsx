import React from 'react'
import { User } from 'models'
import clsx from 'clsx'

interface ProfileIconProps {
  user: User
  size?: 'sm' | 'md' | 'lg'
  variant?: 'dark' | 'light',
  className?: string
}

enum ProfileIconSize {
  sm = 'h-8 w-8 text-sm',
  md = 'h-12 w-12 text-xl',
  lg = 'h-16 w-16 text-2xl'
}

enum ProfileIconVariant {
  light = 'bg-gray-200 text-gray-700',
  dark = 'bg-primary-500 text-gray-200'
}

const getInitials = (user: User) => {
  const firstnameInitial = user.firstname.charAt(0) || ''
  const lastnameInitial = user.lastname.charAt(0) || ''
  return firstnameInitial.concat(lastnameInitial)
}

const ProfileIcon = ({ user, size = 'md', variant = 'dark', className }: ProfileIconProps) => {
  return (
    <div className={clsx(
      'flex justify-center items-center font-bold shadow-md rounded-full opacity-100 tracking-wide select-none',
      ProfileIconSize[size],
      ProfileIconVariant[variant],
      className
    )}>
      <div className="text-center">
        {getInitials(user)}
      </div>
    </div>
  )
}

export default ProfileIcon
