import React, { ChangeEvent, useEffect, useMemo, useRef } from 'react'
import { path } from 'ramda'
import clsx from 'clsx'
import { DeepMap, FieldError } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { getValidDate, dateParser, DateFields } from './utils'

type DateInputProps<T> = {
  name: string
  label?: string
  className?: string
  defaultValue?: string // YYYY-MM-DD Format
  beforeToday?: boolean
  errors: DeepMap<T, FieldError>
  setValue: (name: string, value: string) => void,
  register: (name: string) => void
}

const DateInput = (<T, >({
  name,
  label,
  errors,
  className,
  defaultValue,
  register,
  setValue
}: DateInputProps<T>) => {
  const { t } = useTranslation('date')
  const error = path<string>([name, 'message'], errors)

  const yearInputRef = useRef<HTMLInputElement>(null)
  const monthInputRef = useRef<HTMLInputElement>(null)
  const dayInputRef = useRef<HTMLInputElement>(null)

  const defaults = dateParser(defaultValue)

  const yearName = `${name}Year`
  const monthName = `${name}Month`
  const dayName = `${name}Day`

  const fields = useMemo<DateFields[]>(() => [
    { name: dayName, type: 'day', ref: dayInputRef, placeholder: t('DAY_FORMAT') },
    { name: monthName, type: 'month', ref: monthInputRef, placeholder: t('MONTH_FORMAT') },
    { name: yearName, type: 'year', ref: yearInputRef, placeholder: t('YEAR_FORMAT') }
  ], [dayName, monthName, yearName, t])

  useEffect(() => {
    register(name)
  }, [name, register])

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const tName = e.target.name
    const tValue = e.target.value

    const yearValue = yearInputRef.current?.value
    const monthValue = monthInputRef.current?.value
    const dayValue = dayInputRef.current?.value

    const isComplete = yearValue && monthValue && dayValue

    switch (tName) {
      case (yearName):
        e.target.value = getValidDate(tValue, 3000)
        break
      case (monthName):
        e.target.value = getValidDate(tValue, 12)
        break
      case (dayName):
        e.target.value = getValidDate(tValue, 31)
        break
      default:
        break
    }
    if (isComplete) {
      const stringDate = `${yearValue}-${monthValue}-${dayValue}`
      setValue(name, stringDate)
    }
  }

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Backspace' || e.key === 'Delete') {
      (e.target as HTMLInputElement).value = ''
    }
  }

  return (
    <div className={clsx('mb-3', className)}>
      {label && <label className="block text-gray-700 mb-2" htmlFor={name}>{label}</label>}
      <div className={clsx([
        'flex form-input shadow-lg w-full p-0 focus:shadow-outline mb-1',
        error ? 'border-red-500' : 'border-primary-500'
      ])}>
        {fields.map((field, index) => (
          <div key={field.name} className={clsx(['flex-1', index !== fields.length - 1 && 'border-r'])}>
            <input
              className="focus:outline-none w-full p-2 rounded text-center"
              ref={field.ref}
              onChange={handleChange}
              defaultValue={defaults[field.type]}
              placeholder={field.placeholder}
              type="number"
              name={field.name}
              onKeyDown={handleKeyDown} />
          </div>
        ))}
      </div>
      {error && <small className="block text-red-500">{error}</small>}
    </div>
  )
})

export default DateInput
