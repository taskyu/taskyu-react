interface ParsedDate {
  year?: string
  month?: string
  day?: string
}

export interface DateFields {
  name: string
  type: 'year' | 'month' | 'day'
  ref: React.RefObject<HTMLInputElement>
  placeholder: string
}

export const getValidDate = (value: string, max: number, min = 0): string => {
  const intValue = parseInt(value, 10)
  let stringValue
  if (intValue > max) {
    stringValue = max.toString(10)
  } else if (intValue < min) {
    stringValue = min.toString(10).length === 1 ? '0'.concat(min.toString(10)) : min.toString(10)
  } else {
    stringValue = value.length === 1 ? '0'.concat(value) : value
  }

  if (stringValue.match(/^0+$/g)) {
    return ''
  }

  stringValue = stringValue.replace(/\D/g, '')

  if (stringValue[0] === '0' && stringValue.length > 2) {
    stringValue = stringValue.substring(1)
  }

  return stringValue
}

export const dateParser = (stringDate?: string, format: 'YYYY-MM-DD' | 'DD-MM-YYYY' = 'YYYY-MM-DD'): ParsedDate => {
  const splitted = stringDate ? stringDate.split('-') : undefined
  if (splitted && splitted.length === 3) {
    return format === 'YYYY-MM-DD'
      ? { year: splitted[0], month: splitted[1], day: splitted[2] }
      : { year: splitted[2], month: splitted[1], day: splitted[0] }
  }
  return {}
}

export const dateStringTransformer = (_value: string, originalValue: string) => {
  return new Date(originalValue)
}
