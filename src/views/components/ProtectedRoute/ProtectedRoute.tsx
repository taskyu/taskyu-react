import React, { useEffect } from 'react'
import { Route, RouteProps, useHistory } from 'react-router'
import { useAuthContext } from 'views/contexts/AuthContext/AuthContext'
import withPageAnimation from 'views/hoc/withPageAnimation'

interface ProtectedRouteProps extends RouteProps {
  type?: 'private' | 'public-only'
  scope?: 'none' | 'owner' | 'member'
  Page: React.ComponentType<unknown>
}

const ProtectedRoute = ({ type = 'private', scope = 'none', Page, ...props } : ProtectedRouteProps) => {
  const { isAuthenticated } = useAuthContext()
  const { push } = useHistory()

  useEffect(() => {
    switch (type) {
      case 'public-only':
        if (isAuthenticated) {
          push('/')
        }
        break
      case 'private':
      default:
        if (!isAuthenticated) {
          push('/login')
        }
        // TODO: Apply scopes!
        console.log(scope)
    }
  }, [isAuthenticated, type, push, scope])

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Route component={withPageAnimation(Page)} {...props} />
  )
}

export default ProtectedRoute
