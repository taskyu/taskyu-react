import { StylesConfig } from 'react-select'
import { ThemeConfig } from 'react-select/src/theme'
import { getColor } from 'utils/tailwindConfig'
import chroma from 'chroma-js'

const primaryColor = getColor('primary')
const grayColor = getColor('gray')

export const selectTheme: ThemeConfig = theme => {
  return {
    ...theme,
    colors: {
      ...theme.colors,
      primary: primaryColor('500'),
      primary75: primaryColor('400'),
      primary50: primaryColor('300'),
      primary25: primaryColor('200')
    }
  }
}

export const navbarSelectStyle: StylesConfig = {
  option: styles => ({
    ...styles,
    cursor: 'pointer'
  }),
  control: styles => ({
    ...styles,
    cursor: 'pointer',
    backgroundColor: chroma(grayColor('700')).alpha(0.5).css(),
    border: `.1rem solid ${grayColor('200')}`
  }),
  noOptionsMessage: styles => ({
    ...styles,
    backgroundColor: chroma(grayColor('300')).alpha(0.5).css(),
    color: grayColor('700'),
    textOverflow: 'ellipsis',
    overflow: 'hidden'
  }),
  input: styles => ({
    ...styles,
    color: grayColor('200')
  }),
  singleValue: styles => ({
    ...styles,
    color: grayColor('200')
  }),
  placeholder: styles => ({
    ...styles,
    color: grayColor('400')
  }),
  dropdownIndicator: styles => ({
    ...styles,
    color: grayColor('200')
  })
}
