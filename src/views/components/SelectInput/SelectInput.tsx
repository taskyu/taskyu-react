import React from 'react'
import Select, { Props } from 'react-select'
import { selectTheme } from './styles'

const SelectInput = ({ ...props }: Props) => {
  return (
    <Select
      theme={selectTheme}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props} />
  )
}

export default SelectInput
