import React, { ReactNode } from 'react'
import { path } from 'ramda'
import clsx from 'clsx'
import { DeepMap, FieldError } from 'react-hook-form'

type RefReturn =
  | string
  | ((instance: HTMLInputElement | null) => void)
  | React.RefObject<HTMLInputElement>
  | null
  | undefined

type CheckboxInputProps<T> = {
  name: string
  label?: string
  children?: ReactNode
  className?: string
  checked?: boolean
  defaultChecked?: boolean
  errors: DeepMap<T, FieldError>
  register: () => RefReturn
}

const CheckboxInput = (<T, >({
  name,
  label,
  errors,
  className,
  checked,
  defaultChecked,
  register,
  children
}: CheckboxInputProps<T>) => {
  const error = path<string>([name, 'message'], errors)
  return (
    <div className={clsx('mb-3', className)}>
      <label className="flex items-center mb-2">
        <input
          type="checkbox"
          className={clsx([
            'cursor-pointer form-checkbox text-primary-500 h-6 w-6 shadow-md',
            error && 'border-red-500'
          ])}
          defaultChecked={defaultChecked}
          ref={register}
          name={name}
          checked={checked} />
        <span
          className="ml-3 select-none text-gray-700">
          {label || children}
        </span>
      </label>
      {error && <small className="block text-red-500">{error}</small>}
    </div>
  )
})

export default CheckboxInput
